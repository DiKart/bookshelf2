package com.rfl.bookshelf.dao;

import org.hibernate.query.Query;
import com.rfl.bookshelf.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDaoImpl implements BookDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addBook(Book book) {
        if (!book.getTitle().isEmpty()) {
            Session session = sessionFactory.getCurrentSession();
            session.persist(book);
        }
    }

    @Override
    public void updateBook(Book book) {
        if (!book.getTitle().isEmpty()) {
            Session session = sessionFactory.getCurrentSession();
            session.update(book);
        }
    }

    @Override
    public void deleteBook(int id) {
        Session session = sessionFactory.getCurrentSession();
        Book book = (Book) session.load(Book.class, id);

        if (book != null) {
            session.delete(book);
        }
    }

    @Override
    public void makeAsRead(boolean isUpdate, Book book) {
        Session session = sessionFactory.getCurrentSession();

        if (isUpdate)
            book.setReadAlready(false);
        else
            book.setReadAlready(true);
        session.update(book);
    }

    @Override
    public Book getBookById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Book book = session.load(Book.class, id);
        return book;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> listBooks(int rowsPerPage, int page, String criteria) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Book where title like :criteria")
                .setParameter("criteria", criteria + "%")
                .setFirstResult((page - 1) * 10)
                .setMaxResults(10);

        List<Book> listBooks = (List<Book>) query.list();

        return listBooks;
    }

    @Override
    @SuppressWarnings("unchecked")
    public long getPageCount(String criteria) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Book where title like :criteria")
                .setParameter("criteria", criteria + "%");
        List<Book> list = (List<Book>) query.list();

        long count = list.size();

        return count/10 + ((count%10)== 0? 0:1);
    }

}
