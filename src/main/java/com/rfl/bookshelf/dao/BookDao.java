package com.rfl.bookshelf.dao;


import com.rfl.bookshelf.model.Book;

import java.util.List;

public interface BookDao {
    void addBook(Book book);

    void updateBook(Book book);

    void deleteBook(int id);

    void makeAsRead(boolean isUpdate, Book book);

    Book getBookById(int id);

    List<Book> listBooks( int rowsPerPage, int page, String criteria);

    long getPageCount(String criteria);

}
