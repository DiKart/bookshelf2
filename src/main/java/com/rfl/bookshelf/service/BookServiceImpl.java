package com.rfl.bookshelf.service;

import com.rfl.bookshelf.dao.BookDao;
import com.rfl.bookshelf.model.Pagination;
import com.rfl.bookshelf.model.Book;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookDao bookDao;
    private Pagination pagination;

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    @Transactional
    public void addBook(Book book) {
        bookDao.addBook(book);
    }

    @Override
    @Transactional
    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    @Override
    @Transactional
    public void removeBook(int id) {
        bookDao.deleteBook(id);
    }

    @Override
    @Transactional
    public void makeRead(boolean isUpdate, Book book) {
        bookDao.makeAsRead(isUpdate, book);
    }

    @Override
    @Transactional
    public Book getBookById(int id) {
        return bookDao.getBookById(id);
    }

    @Override
    @Transactional
    public List<Book> listBooks( int rowsPerPage, int page, String criteria) {
        return bookDao.listBooks(rowsPerPage, page, criteria);
    }

    @Override
    @Transactional
    public long getPageCount(String criteria) {
        return bookDao.getPageCount(criteria);
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Pagination getPagination() {
        return pagination;
    }
}

