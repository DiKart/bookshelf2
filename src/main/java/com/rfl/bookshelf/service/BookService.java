package com.rfl.bookshelf.service;

import com.rfl.bookshelf.model.Book;

import java.util.List;

public interface BookService {
    void addBook(Book book);

    void updateBook(Book book);

    void removeBook(int id);

    void makeRead(boolean isUpdate, Book book);

    Book getBookById(int id);

    List<Book> listBooks( int rowsPerPage, int page, String criteria);

    long getPageCount(String criteria);
}
