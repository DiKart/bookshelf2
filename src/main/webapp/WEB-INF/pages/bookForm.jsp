<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>


<head>

    <c:if test="${empty book.title}">
        <title>Добавление книги</title>
    </c:if>

    <c:if test="${!empty book.title}">
        <title>Редактирование книги</title>
    </c:if>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">


    <c:if test="${empty book.title}">
        <h1>Добавить новую книгу</h1>
    </c:if>

    <c:if test="${!empty book.title}">
        <h1>Редактирование книги</h1>
    </c:if>


    <c:url var="addAction" value="/books/add"/>

    <form:form action="${addAction}" modelAttribute="book">
        <table class="table table-bordered">
            <c:if test="${!empty book.title}">
                <tr>
                    <td>
                        <form:label path="id">
                            <spring:message text="ID"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="id" readonly="true" disabled="true"/>
                        <form:hidden path="id"/>
                    </td>
                </tr>
            </c:if>
            <tr>
                <td>
                    <form:label path="title">
                        <spring:message text="Название книги (не пустое)"/>

                    </form:label>
                </td>
                <td>
                    <form:input maxlength="100" path="title"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="description">
                        <spring:message text="Описание"/>
                    </form:label>
                </td>
                <td>
                    <form:input maxlength="255" path="description"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="author">
                        <spring:message text="Автор"/>
                    </form:label>
                </td>
                <c:if test="${!empty book.title}">
                    <td>
                        <form:input path="author" readonly="true" disabled="true"/>
                        <form:hidden path="author"/>
                    </td>
                </c:if>
                <c:if test="${empty book.title}">
                    <td>
                        <form:input maxlength="100" path="author"/>
                    </td>
                </c:if>
            </tr>
            <tr>
                <td>
                    <form:label path="isbn">
                        <spring:message text="ISBN"/>
                    </form:label>
                </td>
                <td>
                    <form:input maxlength="20" path="isbn"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="printYear">
                        <spring:message text="Год издания"/>
                    </form:label>
                </td>
                <td>
                    <form:input maxlength="4" path="printYear"/>
                </td>
            </tr>
            <tr>
                <c:if test="${book.readAlready == false}">
            <tr>
                <td>
                    <form:label path="readAlready">
                        <spring:message text="Прочитана"/>
                    </form:label>
                </td>
                <td>
                    <form:checkbox path="readAlready"/>
                </td>
            </tr>
            </c:if>

            </tr>
            <tr>
                <td colspan="2">
                    <c:if test="${!empty book.title}">
                        <input type="submit" class="btn btn-primary"
                               value="<spring:message text="Сохранить изменения"/>"/>
                    </c:if>
                    <c:if test="${empty book.title}">
                        <input type="submit" class="btn btn-primary"
                               value="<spring:message text="Добавить книгу"/>"/>
                    </c:if>
                </td>
            </tr>
        </table>
    </form:form>
    <a href="<c:url value="/${pagination.page}"/>" class="btn btn-default">Назад</a>
</div>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
