package com.rfl.bookshelf.controller;

import com.rfl.bookshelf.model.Pagination;
import com.rfl.bookshelf.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.rfl.bookshelf.model.Book;
import com.rfl.bookshelf.service.Search;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;


@Controller
public class BookController {

    private BookService bookService;
    private Search tmpSearch = new Search();
    private Pagination pagination;

    @Autowired
    public void setBookService(BookService bookService){
        this.bookService = bookService;
    }
    @Autowired
    public void setPagination(Pagination pagination){
        this.pagination = pagination;
    }

    @RequestMapping(value = {"*", "/{page}"})
    public String listBooks(@PathVariable Optional<String> page, Model model) {
        if (page.isPresent()) {
            pagination.setPage(Integer.parseInt(page.get()));
        }else
            pagination.setPage(1);
        pagination.setPageCount(bookService.getPageCount(tmpSearch.getSearchString()));
        model.addAttribute("pageList", pagination.getPageList());
        model.addAttribute("pagination", pagination);
        List<Book> bookList = bookService.listBooks(
                pagination.getRowsPerPage(),
                pagination.getPage(),
                tmpSearch.getSearchString());
        pagination.setBookOnPage(bookList.size());
        model.addAttribute("listBooks", bookList);
        model.addAttribute("search", new Search());
        return "index";
    }

    @RequestMapping(value = "/bookForm")
    public String bookForm(@ModelAttribute("book") Book book, Model model){
        model.addAttribute("pagination", pagination);
        model.addAttribute("book", book);
        return "bookForm";
    }

    @RequestMapping(value = "/books/add", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("book") Book book) {
        if(book.getId() == 0) {
            this.bookService.addBook(book);
            pagination.setPage(1);
        } else {
            this.bookService.updateBook(book);

        }
        return "redirect:/" + pagination.getPage();
    }

    @RequestMapping(value = "/refresh")
    public String refresh() {
        pagination.setPage(1);
        tmpSearch.setSearchString("");
        return "redirect:/" + pagination.getPage();
    }

    @RequestMapping (value = "edit/{id}")
    public String updateBook(@PathVariable ("id") int id, Model model) {
        model.addAttribute("pagination", pagination);
        Book book = bookService.getBookById(id);
        book.setReadAlready(false);
        bookService.updateBook(book);
        model.addAttribute("book", book);
        return "bookForm";
    }
    @RequestMapping("remove/{id}")
    public String deleteBook(@PathVariable("id") int id, Model model) {
        this.bookService.removeBook(id);
        if(pagination.getBookOnPage() == 0) {
            pagination.setPage(pagination.getPage() - 1);
        }
        return "redirect:/" + (pagination.getPage());
    }


    @RequestMapping(value = "books/search", method = RequestMethod.POST)
    public String search(@ModelAttribute(value = "search") Search search, Model model) {
        pagination.setPage(1);
        tmpSearch.setSearchString(search.getSearchString());
        List<Book> bookList = bookService.listBooks(
                pagination.getRowsPerPage(),
                pagination.getPage(),
                tmpSearch.getSearchString());
        pagination.setBookOnPage(bookList.size());
        model.addAttribute("listBooks", bookList);
        return "redirect:/" + pagination.getPage();
    }

    @RequestMapping("/bookData/{id}")
    public String bookData(@PathVariable("id") int id, Model model){
        model.addAttribute("pagination", pagination);
        Book book = bookService.getBookById(id);
        book.setReadAlready(true);
        bookService.updateBook(book);
        model.addAttribute("book", book);
        return "bookData";
    }

}
