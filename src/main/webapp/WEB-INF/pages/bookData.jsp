<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Просмотр</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
</head>

<body>
<div class="container">



    <div class="col-md-8">
        <h1>Подробная информация о книге</h1>
        <table class="table table-bordered">
            <tr>
                <th width="120">ID</th>
                <td>${book.id}</td>
            </tr>
            <tr>
                <th width="120">Название</th>
                <td>${book.title}</td>
            </tr>
            <tr>
                <th width="120">Автор</th>
                <td>${book.author}</td>
            </tr>
            <tr>
                <th width="120">ISBN</th>
                <td>${book.isbn}</td>
            </tr>
            <tr>
                <th width="120">Год издания</th>
                <td>${book.printYear}</td>
            </tr>
            <tr>
                <th width="120">Прочитана</th>
                <td>${book.readAlready}</td>
            </tr>

        </table>
    </div>
    <div class="col-md-4">
        <p>
        <h2>Описание</h2>
        <textarea maxlength="255" rows="12" cols="50" readonly>${book.description}</textarea>
        </p>
    </div>
</div>
<div class="container">
    <a href="<c:url value="/${pagination.page}"/>" class="btn btn-default">Назад</a>
</div>

<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>