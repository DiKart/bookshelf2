package com.rfl.bookshelf.model;


import org.springframework.stereotype.Service;

@Service
public class Pagination {
    private int page;
    private int rowsPerPage;
    private int pagesPerView;
    private long pageCount;
    private int bookOnPage;
    private int pageList;

    public int getPageList() {
        if(page % pagesPerView == 0){
            return page / pagesPerView -1;
        }else {
            return page / pagesPerView;
        }
    }

    public int getBookOnPage() {
        return bookOnPage;
    }

    public void setBookOnPage(int bookOnPage) {
        this.bookOnPage = bookOnPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getPagesPerView() {
        return pagesPerView;
    }

    public void setPagesPerView(int pagesPerView) {
        this.pagesPerView = pagesPerView;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }
}
