<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book shelf</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>

</head>
<body>
<div class="container">
    <h1>Список книг</h1>

    <c:url var="addAction" value="/books/search"/>


        <form action="<c:url value="/books/search"/>" method="post">
            <input type="text" name="searchString" value="${search.searchString}" placeholder="Название"/>
            <input type="submit" class="btn btn-primary" name="search" id="1" value="Поиск"/>
            <a href="<c:url value="/refresh"/>"><input type="button" class="btn btn-default"
                                                       name="clear" id="2" value="Очистить"/></a>
            <a href="<c:url value="/bookForm"/>"><input type="button" class="btn btn-success"
                                                        name="addBook" id="3" value="Добавить книгу"></a>
        </form>


    <c:if test="${!empty listBooks}">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th width="80">ID</th>
                <th width="120">Название</th>
                <th width="120">Автор</th>
                <th width="120">ISBN</th>
                <th width="120">Год издания</th>
                <th width="80">Прочитана</th>
                <th width="60">Редактировать</th>
                <th width="60">Удалить</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listBooks}" var="book">
                <tr>
                    <td>${book.id}</td>
                    <td><a href="/bookData/${book.id}">${book.title}</a></td>
                    <td>${book.author}</td>
                    <td>${book.isbn}</td>
                    <td class="text-center">${book.printYear}</td>
                    <td class="text-center"><input type="checkbox" onclick="return false;"
                    <c:if test="${book.readAlready == true}"> checked </c:if> </td>
                    <td><a href="<c:url value='/edit/${book.id}'/>" class="btn btn-info">Редактировать</a></td>
                    <td><a href="<c:url value='/remove/${book.id}'/>" class="btn btn-danger">Удалить</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <div>
        <c:url value="/${pagination.page - 1}" var="prev"/>
        <c:url value="/${pagination.page +1}" var="next"/>

        <c:if test="${pagination.pageList >= 0}">

            <c:if test="${pagination.page > 1}">
                <a href="<c:out value="${prev}" />">пред</a>
            </c:if>

            <c:forEach begin="1" end="${pagination.pagesPerView}" step="1" varStatus="i">
                <c:set var="p" value="${pagination.pageList * pagination.pagesPerView + i.index}"/>
                <c:if test="${p <= pagination.pageCount}">
                    <c:choose>
                        <c:when test="${pagination.page == p}">
                            <span>${p}</span>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/${p}" var="url"/>
                            <a href="<c:out value="${url}"/>">${p}</a>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>

            <c:if test="${pagination.page < pagination.pageCount}">
                <a href="<c:out value="${next}" />">след</a>
            </c:if>

        </c:if>


    </div>

    <p>
        Всего страниц: ${pagination.pageCount}
    </p>
</div>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
